Here is a list of appliations recommended to install on Ubuntu.

1. **Latex Editor**

    > [**Texstudio**](http://texstudio.sourceforge.net) is a cross-platform application, and easy to use. It is developed with Qt, and open source, so has the potential to customize/enhance it easily.


2. **Text Editor/Viewer**

    > [**Atom**](https://atom.io/) is a modern text editor, which has similar features as Sublime and Textmate. It has a lot of features, and is easy to be custmozied. It is developed using Node.js, and is actively developed now. Moreover, it is totally free, for both zero-cost and freedom.


3. **PDF Viewer**

    > [**qpdfview**](https://apps.ubuntu.com/cat/applications/qpdfview) is availabe in Ubuntu Software Center. It is developed in Qt, and is super fast.
