# How to use Microsoft OneDrive on Ubuntu?

## Firstly, download an open source version of [Onedrive-d](https://github.com/xybu/onedrive-d/archive/master.zip).

## Secondly, install it.
```shell
$ unzip master.zip
$ cd onedrive-d-master
$ sudo ./inst install
# if update, then execute reinstall instead.
$ sudo ./inst reinstall
```

## At last, configure it.
```shell
$ onedrive-prefs
```

### 1. Configure the preference as following.

![preference](preference.png)

### 2. Click "Connect to OneDrive.com", and popup the following window.

![account](account.png)

### 3. Click "Sign in", and popup the following window for granting permissions to onedrive-d.

![permission](permission.png)

### 4. Once configured, the following window will appear.

![result](result.png)

## Start your onedrive-d after rebooting.
```shell
$ onedrive-d &
```

